package pl.sda.quizwebsite;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.sda.quizwebsite.repositories.QuestionRepository;

import java.util.logging.Logger;


@SpringBootApplication
public class QuizWebsiteApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuizWebsiteApplication.class, args);



    }

}


