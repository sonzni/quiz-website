package pl.sda.quizwebsite;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.sda.quizwebsite.entities.Answer;
import pl.sda.quizwebsite.entities.AnswerSet;
import pl.sda.quizwebsite.entities.Category;
import pl.sda.quizwebsite.entities.Question;
import pl.sda.quizwebsite.repositories.AnswerRepository;
import pl.sda.quizwebsite.repositories.AnswerSetRepository;
import pl.sda.quizwebsite.repositories.CategoryRepository;
import pl.sda.quizwebsite.repositories.QuestionRepository;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class DataLoader {

    @Autowired
    QuestionRepository questionRepository;
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    AnswerSetRepository answerSetRepository;
    @Autowired
    AnswerRepository answerRepository;

    public DataLoader(){
    }

    @PostConstruct
    public void init(){
        System.out.println("Database operations");

        //dodanie pierwszego pytania i odpowiedzi:
        Question myQuestion = new Question("Spośród wymienionych, malarzem surrealistycznym nie był:");
        myQuestion.setAnswers(new AnswerSet());

        Answer ans1 = new Answer("Pablo Picasso", true);
        ans1.setAnswerSet(myQuestion.getAnswers());

        Answer ans2 = new Answer("Salvador Dali", false);
        ans2.setAnswerSet(myQuestion.getAnswers());

        myQuestion.getAnswers().getAnswers().add(ans1);
        myQuestion.getAnswers().getAnswers().add(ans2);

        questionRepository.save(myQuestion);

        //dodanie drugiego pytania i odpowiedzi:
        Question myQuestion2 = new Question("Kto namalował Ostatnią Wieczerzę?");
        myQuestion2.setAnswers(new AnswerSet());

        Answer ans3 = new Answer("Michał Anioł", false);
        ans3.setAnswerSet(myQuestion2.getAnswers());
        Answer ans4 = new Answer("Leonardo da Vinci", true);
        ans4.setAnswerSet(myQuestion2.getAnswers());

        myQuestion2.getAnswers().getAnswers().add(ans3);
        myQuestion2.getAnswers().getAnswers().add(ans4);

        questionRepository.save(myQuestion2);

        //dodanie trzeciego pytania i odpowiedzi:
        Question myQuestion3 = new Question("Alfons Mucha to znany czeski malarz i grafik. W jakiej epoce tworzył?");
        myQuestion3.setAnswers(new AnswerSet());

        Answer ans5 = new Answer("w rokoko", false);
        ans5.setAnswerSet(myQuestion3.getAnswers());
        Answer ans6 = new Answer("w secesji", true);
        ans6.setAnswerSet(myQuestion3.getAnswers());

        myQuestion3.getAnswers().getAnswers().add(ans5);
        myQuestion3.getAnswers().getAnswers().add(ans6);

        questionRepository.save(myQuestion3);

        //4:
        Question myQuestion4 = new Question("Najwyższa góra świata to:");
        myQuestion4.setAnswers(new AnswerSet());

        Answer ans7 = new Answer("Mont Blanc", false);
        ans7.setAnswerSet(myQuestion4.getAnswers());
        Answer ans8 = new Answer("Mount Everest", true);
        ans8.setAnswerSet(myQuestion4.getAnswers());

        myQuestion4.getAnswers().getAnswers().add(ans7);
        myQuestion4.getAnswers().getAnswers().add(ans8);

        questionRepository.save(myQuestion4);

        //5:
        Question myQuestion5 = new Question("Najwyższa góra świata to:");
        myQuestion5.setAnswers(new AnswerSet());

        Answer ans9 = new Answer("Mont Blanc", false);
        ans9.setAnswerSet(myQuestion5.getAnswers());
        Answer ans10 = new Answer("Mount Everest", true);
        ans10.setAnswerSet(myQuestion5.getAnswers());

        myQuestion5.getAnswers().getAnswers().add(ans9);
        myQuestion5.getAnswers().getAnswers().add(ans10);

        questionRepository.save(myQuestion5);

        //6:
        Question myQuestion6 = new Question("Z którym oceanem ma styczność wschodnie wybrzeże Stanów Zjednoczonych?");
        myQuestion6.setAnswers(new AnswerSet());

        Answer ans11 = new Answer("Ocean Atlantycki", true);
        ans11.setAnswerSet(myQuestion6.getAnswers());
        Answer ans12 = new Answer("Ocean Indyjski", false);
        ans12.setAnswerSet(myQuestion6.getAnswers());

        myQuestion6.getAnswers().getAnswers().add(ans11);
        myQuestion6.getAnswers().getAnswers().add(ans12);

        questionRepository.save(myQuestion6);

        //7:
        Question myQuestion7 = new Question("Najdłuższa rzeka w Europie to:");
        myQuestion7.setAnswers(new AnswerSet());

        Answer ans13 = new Answer("Wołga", true);
        ans13.setAnswerSet(myQuestion7.getAnswers());
        Answer ans14 = new Answer("Dunaj", false);
        ans14.setAnswerSet(myQuestion7.getAnswers());

        myQuestion7.getAnswers().getAnswers().add(ans13);
        myQuestion7.getAnswers().getAnswers().add(ans14);

        questionRepository.save(myQuestion7);

        //8:
        Question myQuestion8 = new Question("Pierwszym królem Polski był:");
        myQuestion8.setAnswers(new AnswerSet());

        Answer ans15 = new Answer("Bolesław I Chrobry", true);
        ans15.setAnswerSet(myQuestion8.getAnswers());
        Answer ans16 = new Answer("Mieszko I", false);
        ans16.setAnswerSet(myQuestion8.getAnswers());

        myQuestion8.getAnswers().getAnswers().add(ans15);
        myQuestion8.getAnswers().getAnswers().add(ans16);

        questionRepository.save(myQuestion8);

        //9:
        Question myQuestion9 = new Question("W którym roku miała miejsce bitwa pod Grunwaldem?");
        myQuestion9.setAnswers(new AnswerSet());

        Answer ans17 = new Answer("1410", true);
        ans17.setAnswerSet(myQuestion9.getAnswers());
        Answer ans18 = new Answer("1401", false);
        ans18.setAnswerSet(myQuestion9.getAnswers());

        myQuestion9.getAnswers().getAnswers().add(ans17);
        myQuestion9.getAnswers().getAnswers().add(ans18);

        questionRepository.save(myQuestion9);

        //10:
        Question myQuestion10 = new Question("Katarzyna II Wielka była cesarzową:");
        myQuestion10.setAnswers(new AnswerSet());

        Answer ans19 = new Answer("Prus", false);
        ans19.setAnswerSet(myQuestion10.getAnswers());
        Answer ans20 = new Answer("Rosji", true);
        ans20.setAnswerSet(myQuestion10.getAnswers());

        myQuestion10.getAnswers().getAnswers().add(ans19);
        myQuestion10.getAnswers().getAnswers().add(ans20);

        questionRepository.save(myQuestion10);

        //11:
        Question myQuestion11 = new Question("Który z naukowców odkrył penicylinę?");
        myQuestion11.setAnswers(new AnswerSet());

        Answer ans21 = new Answer("Ludwik Pasteur", false);
        ans21.setAnswerSet(myQuestion11.getAnswers());
        Answer ans22 = new Answer("Aleksander Fleming", true);
        ans22.setAnswerSet(myQuestion11.getAnswers());

        myQuestion11.getAnswers().getAnswers().add(ans21);
        myQuestion11.getAnswers().getAnswers().add(ans22);

        questionRepository.save(myQuestion11);

        //12:
        Question myQuestion12 = new Question("W skład układu pokarmowego człowieka nie wchodzi:");
        myQuestion12.setAnswers(new AnswerSet());

        Answer ans23 = new Answer("Wątroba", false);
        ans23.setAnswerSet(myQuestion12.getAnswers());
        Answer ans24 = new Answer("Trzustka", true);
        ans24.setAnswerSet(myQuestion12.getAnswers());

        myQuestion12.getAnswers().getAnswers().add(ans23);
        myQuestion12.getAnswers().getAnswers().add(ans24);

        questionRepository.save(myQuestion12);

        //13:
        Question myQuestion13 = new Question("Ichtiologia to dział zoologii zajmujący się:");
        myQuestion13.setAnswers(new AnswerSet());

        Answer ans25 = new Answer("Płazami", false);
        ans25.setAnswerSet(myQuestion13.getAnswers());
        Answer ans26 = new Answer("Rybami", true);
        ans26.setAnswerSet(myQuestion13.getAnswers());

        myQuestion13.getAnswers().getAnswers().add(ans25);
        myQuestion13.getAnswers().getAnswers().add(ans26);

        questionRepository.save(myQuestion13);

        //
        Category category1 = new Category("Sztuka");
      //  category1.setCategoryName(category1);
        







        //questionRepository.save(new Question("Spośród wymienionych, malarzem surrealistycznym nie był:"));
        //questionRepository.save(new Question("Kto namalował Ostatnią Wieczerzę?"));
        //questionRepository.save(new Question("Alfons Mucha to znany czeski malarz i grafik. W jakiej epoce tworzył?"));
       // questionRepository.save(new Question("Najwyższa góra świata to:"));
        //questionRepository.save(new Question("Z którym oceanem ma styczność wschodnie wybrzeże Stanów Zjednoczonych?"));
       // questionRepository.save(new Question("Najdłuższa rzeka w Europie to:"));
        //questionRepository.save(new Question("Pierwszym królem Polski był:"));
       // questionRepository.save(new Question("W którym roku miała miejsce bitwa pod Grunwaldem?"));
       // questionRepository.save(new Question("ataryzna II Wielka była cesarzową:"));
        //questionRepository.save(new Question("Który z naukowców odkrył penicylinę?"));
        //questionRepository.save(new Question("W skład układu pokarmowego człowieka nie wchodzi:"));
        //questionRepository.save(new Question("Ichtiologia to dział zoologii zajmujący się:"));
        questionRepository.findAll();

        answerRepository.save(new Answer("Pablo Picasso", true));
        answerRepository.save(new Answer("Leonardo da Vinci'", true));
        answerRepository.save(new Answer("w secesji", true));
        answerRepository.save(new Answer("Mount Everest", true));
        answerRepository.save(new Answer("Ocean Atlantycki", true));
        answerRepository.save(new Answer("Wołga", true));
        answerRepository.save(new Answer("Bolesław I Chrobry", true));
        answerRepository.save(new Answer("1410", true));
        answerRepository.save(new Answer("Rosji", true));
        answerRepository.save(new Answer("Aleksander Fleming", true));
        answerRepository.save(new Answer("Trzustka", true));
        answerRepository.save(new Answer("Rybami", true));



        categoryRepository.save(new Category("Sztuka"));
        categoryRepository.save(new Category("Geografia"));
        categoryRepository.save(new Category("Historia"));
        categoryRepository.save(new Category("Biologia"));


        categoryRepository.findAll();
}



//Task save(Task t) – zapisz task do bazy danych
//Iterable save(Iterable t) – zapisanie kolekcji obiektów
//Task findOne(Long id) – znajduje wpis z kluczem podanym jako parametr
//boolean exists(Long id) – sprawdź czy wpis z kluczem istnieje
//Iterable findAll() – pobierz wszystkie wpisy z bazy danych
//Iterable findAll(Iterable IDs) – znajdź wszystkie elementy z kluczami na liście IDs
//long count() – policz elementy w tabeli
//void delete(Long id) – usuń element z kluczem id
//void delete(Task r) – usuń obiekt z tabelki
//void delete(Iterable IDs) – usuń wszystkie obiekty których klucze znajdują się na liście IDs
//deleteAll() – wyczyść tabelkę




}
