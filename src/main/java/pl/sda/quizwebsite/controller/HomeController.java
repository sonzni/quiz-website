package pl.sda.quizwebsite.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.quizwebsite.services.CategoryService;
import pl.sda.quizwebsite.services.CategoryServiceImpl;

import java.util.HashMap;
import java.util.Map;

@Controller
public class HomeController {

    @GetMapping("/home")
    public String home(){
        return "home";
    }



    @Autowired
    private CategoryServiceImpl categoryService;

    @RequestMapping("/")
    public String index(Model model) {
        return "index";
    }

    @RequestMapping("/category")
    public ModelAndView showCategories(){
        var categories = categoryService.findAll();

        Map<String, Object> params = new HashMap<>();
        params.put("category", categories);

        return new ModelAndView("showCategories", params);
    }


}

//	@Resource
//	QuizGenerator quizGenerator;
//
//	@Resource
//	QuizConverter quizConverter;
//
//	@Resource(name = "categoryRepository")
//	CategoryRepository categoryRepository;
//
//	@RequestMapping(value="/quiz", method = RequestMethod.GET)
//	public String generateQuiz2(@RequestParam String categoryName, ModelMap model){
//
//		//Pobieranie danych z bazy
//		Category category = categoryRepository.findByName(categoryName);
//
//		//faza 2 pytania przekonwertować np, do obiektu typu Quiz
//		Quiz quiz = quizConverter.getQuiz(category);
//
//		model.put("quiz", quiz);
//
//		//Prezentacja na froncie
//		return "quiz";
//	}
//}
