package pl.sda.quizwebsite.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
@Controller
public class HistoryController {

    @GetMapping("/historia")
    public String history(){
        return "history";
    }
}