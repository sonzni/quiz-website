package pl.sda.quizwebsite.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.quizwebsite.services.QuestionServiceImpl;

import java.util.HashMap;
import java.util.Map;

@Controller
public class BiologyController {


   @GetMapping("/biologia")
    public String biology() {
       return "biology";
   }

}
