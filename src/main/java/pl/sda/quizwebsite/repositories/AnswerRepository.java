package pl.sda.quizwebsite.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.quizwebsite.entities.Answer;
import pl.sda.quizwebsite.entities.Question;

@Repository
public interface AnswerRepository extends JpaRepository <Answer, Integer> {
}
