package pl.sda.quizwebsite.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.quizwebsite.entities.AnswerSet;
import pl.sda.quizwebsite.entities.Question;

@Repository
public interface AnswerSetRepository extends JpaRepository <AnswerSet, Integer> {
}
