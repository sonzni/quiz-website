package pl.sda.quizwebsite.entities;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class AnswerSet {

    @Id
    @GeneratedValue
    private int id;

    @OneToOne(mappedBy ="answers")
    @Cascade(org.hibernate.annotations.CascadeType.PERSIST)
    private Question question;

    @Column
    @OneToMany(mappedBy = "answerSet")
    @Cascade(org.hibernate.annotations.CascadeType.PERSIST)
    private Set<Answer> answers = new HashSet<>();

    public AnswerSet() {
    }

    public Set<Answer> getAnswers() {
        return answers;
    }

    //public AnswerSet(Set<Answer> answers) {
    //this.answers = answers;
    // this.answers = new HashSet<>();
    //  for(int i =0; i < answers.size(); i++){
    //this.answers.add(answers[i]);
    // }
    // }
}

//    @OneToMany(mappedBy = "answerSet")
//    @Cascade(CascadeType.PERSIST)
//    private Set<Answer> answers = new HashSet<>();
//
//    public AnswerSet() {
//    }