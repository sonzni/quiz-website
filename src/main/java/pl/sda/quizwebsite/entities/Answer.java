package pl.sda.quizwebsite.entities;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;

@Entity
public class Answer {

    @Id
    @GeneratedValue
    private int id;

    @Column
    private String answer;

    boolean isCorrect;

    public Answer(){

    }

    public Answer(String answer, boolean isCorrect) {
        this.answer = answer;
        this.isCorrect = isCorrect;
    }

    @ManyToOne
    @Cascade(org.hibernate.annotations.CascadeType.PERSIST)
    private AnswerSet answerSet;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }

    public AnswerSet getAnswerSet() {
        return answerSet;
    }

    public void setAnswerSet(AnswerSet answerSet) {
        this.answerSet = answerSet;
    }
}