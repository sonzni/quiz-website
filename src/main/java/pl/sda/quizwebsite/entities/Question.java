package pl.sda.quizwebsite.entities;

import org.hibernate.annotations.Cascade;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;

@Entity
public class Question {

    @Id
    @GeneratedValue
    private int id;

    @Column
    private String questionText;

    @OneToOne
    @Cascade(org.hibernate.annotations.CascadeType.PERSIST)
    private AnswerSet answers;

    @ManyToOne
    @Cascade(org.hibernate.annotations.CascadeType.PERSIST)
    private Category category;


    public Question(){

    }

    public Question(String questionText) {
        this.questionText = questionText;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public AnswerSet getAnswers() {
        return answers;
    }

    public void setAnswers(AnswerSet answers) {
        this.answers = answers;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
