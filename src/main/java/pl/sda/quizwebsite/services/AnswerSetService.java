package pl.sda.quizwebsite.services;
import pl.sda.quizwebsite.entities.AnswerSet;
import pl.sda.quizwebsite.entities.Category;

import java.util.List;

public interface AnswerSetService {

    public void saveAnswerSet(AnswerSet answerSet);

    List<AnswerSet> findAll();

}
