package pl.sda.quizwebsite.services;

import pl.sda.quizwebsite.entities.Question;

import java.util.List;

public interface QuestionService {

    public void saveQuestion(Question question);

    List<Question> findAll();
}
