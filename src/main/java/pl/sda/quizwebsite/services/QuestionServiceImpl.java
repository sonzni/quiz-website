package pl.sda.quizwebsite.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.quizwebsite.entities.Answer;
import pl.sda.quizwebsite.entities.Question;
import pl.sda.quizwebsite.repositories.QuestionRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {


    @Autowired
   private QuestionRepository questionRepository;


    @Override
    public void saveQuestion(Question question) {
    questionRepository.save(question);
    }

    @Override
    public List<Question> findAll() {
        return (List<Question>) questionRepository.findAll();
    }


}

