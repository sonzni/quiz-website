package pl.sda.quizwebsite.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.quizwebsite.entities.Answer;
import pl.sda.quizwebsite.entities.AnswerSet;
import pl.sda.quizwebsite.repositories.AnswerSetRepository;

import java.util.List;

@Service
public class AnswerSetServiceImpl implements AnswerSetService{

    @Autowired
    private AnswerSetRepository answerSetRepository;


    @Override
    public void saveAnswerSet(AnswerSet answerSet) {

    }

    @Override
    public List<AnswerSet> findAll() {
        return (List<AnswerSet>) answerSetRepository.findAll();
    }
}
