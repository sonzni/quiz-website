package pl.sda.quizwebsite.services;


import pl.sda.quizwebsite.entities.Answer;
import pl.sda.quizwebsite.entities.Category;

import java.util.List;

public interface AnswerService {

    public void saveAnswer(Answer answer);

    List<Answer> findAll();


}
