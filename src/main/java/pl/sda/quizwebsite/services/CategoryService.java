package pl.sda.quizwebsite.services;


import pl.sda.quizwebsite.entities.Category;
import pl.sda.quizwebsite.entities.Question;

import java.util.List;

public interface CategoryService {

    public void saveCategory(Category category);

    List<Category> findAll();

}
