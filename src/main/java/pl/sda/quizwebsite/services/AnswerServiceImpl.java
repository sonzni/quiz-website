package pl.sda.quizwebsite.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.quizwebsite.entities.Answer;
import pl.sda.quizwebsite.repositories.AnswerRepository;

import java.util.List;

@Service
public class AnswerServiceImpl implements AnswerService{

    @Autowired
    private AnswerRepository answerRepository;

    @Override
    public void saveAnswer(Answer answer) {

    }

    @Override
    public List<Answer> findAll() {
        return (List<Answer>) answerRepository.findAll();
    }
}