<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Home</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=BebasNeue">
    <style>

        body {
            background-color: #002D83;
            font-family: "Bree Serif";
            font-size: 22px;
            background-image: url("images/quiz-background.png");
            background-repeat: repeat-x;
        }

        h2 {
            color: white;
            text-align: center;
        }
        .button {
            display: inline-block;
            padding: 15px 55px;
            font-size: 18px;
            cursor: pointer;
            text-align: center;
            text-decoration: none;
            outline: none;
            color: #FFFFFF;
            background-color: #FF9B16;
            border: none;
            border-radius: 30px;
            font-family: "Bree Serif";
        }

        .button:hover {background-color: #FFC342}

        .button:active {
            background-color: #FF9B16;
            transform: translateY(3px);
        }

        * {
            box-sizing: border-box;
        }

        .column {
            float: left;

            padding: 50px;

        }
        .row::after {
            content: "";
            clear: both;
            display: table;
        }
        .centered{
            margin-left: auto;
            margin-right: auto;
        }

    </style>
</head>


<body>
<br>
<br>


<div style="text-align: center"><img src="images/quiz-logo-2.png"></div>

<h2><p >Wybierz kategorię:</p></h2>
<br>
<div class="centered">
    <div class="column">
        <img src="images/historia2.png" alt="Historia" style="width:100%">
        <p><button class="button"><a href="historia">Historia</a></button></p>
    </div>
    <div class="column">
        <img src="images/geografia2.png" alt="Geografia" style="width:100%">
        <p><button class="button"><a href="geografia">Geografia</a></button></p>
    </div>
    <div class="column">
        <img src="images/sztuka2.png" alt="Sztuka" style="width:100%">
        <p><button  class="button" align="center"><a href="sztuka">Sztuka</a></button></p>
    </div>
    <div class="column">
        <img src="images/biologia2.png" alt="Biologia" style="width:100%">
        <p><button  class="button" align="center"><a href="biologia">Biologia</a></button></p>
    </div>
</div>
<br>
</body>
</html>
